﻿using System.Collections.Generic;
using UnityEngine;
using System;


public class Scheduler : Singleton<Scheduler>
{
    class TaskInfo
    {
        public Action action;
        public float delay;
    }


    #region Fields

    List<TaskInfo> tasksInfo = new List<TaskInfo>();

    #endregion



    #region Unity Lifecycle

    void Update()
    {
        float deltaTime = Time.deltaTime;

        for(int i = 0; i < tasksInfo.Count; i++)
        {
            tasksInfo[i].delay -= deltaTime;

            if (tasksInfo[i].delay <= 0.0f)
            {
                tasksInfo[i].action?.Invoke();
                tasksInfo.Remove(tasksInfo[i]);
            }
        }
    }

    #endregion



    #region Public Methods

    public void CallMethodWithDelay(Action action, float delay)
    {
        tasksInfo.Add(new TaskInfo { action = action, delay = delay });
    }

    #endregion
}
