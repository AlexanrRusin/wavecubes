﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : class
{
    static T instance;
    static internal Type t = typeof(T);


    public static T Instance { get; private set; }


    protected virtual void Awake()
    {
        Instance = this as T;
    }

}
