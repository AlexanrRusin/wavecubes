﻿using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEngine;

public class BuildPostProcess : MonoBehaviour
{
    [PostProcessBuild]
    public static void AddAdMobIdToPlist(BuildTarget buildTarget, string pathToBuiltProject)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromFile(plistPath);
            plist.root.SetString("GADApplicationIdentifier", "ca-app-pub-2626278881217468~5621754978");// est
            plist.root.SetBoolean("ITSAppUsesNonExemptEncryption",false);
            plist.WriteToFile(plistPath);
        }
    }
}
