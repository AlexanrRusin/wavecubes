﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class MaterialColorInfo
{
    public Material material;
    public Color color;
}

[Serializable]
public class LevelColorsInfo
{
    public List<MaterialColorInfo> materialsColors;
    public Color waterLightColor;
    public Color waterDarkColor;
}


[CreateAssetMenu(menuName = "ScriptableObjects/LevelsColorsSettings", fileName = "LevelsColorsSettings")]
public class LevelsColorsSettings : ScriptableObject
{
    [SerializeField] List<LevelColorsInfo> levelsColorsInfos;

    int lastUsedLevelIndex = -1;


    public LevelColorsInfo GetColorsInfo(int levelIndex)
    {
        while (levelIndex >= levelsColorsInfos.Count)
        {
            levelIndex -= levelsColorsInfos.Count;
        }

        return levelsColorsInfos[levelIndex];
    }


    public void RefreshColors(int levelIndex)
    {
        if (lastUsedLevelIndex != levelIndex)
        {
            while (levelIndex >= levelsColorsInfos.Count)
            {
                levelIndex -= levelsColorsInfos.Count;
            }

            for (int i = 0; i < levelsColorsInfos[levelIndex].materialsColors.Count; i++)
            {
                levelsColorsInfos[levelIndex].materialsColors[i].material.SetColor("_Color", levelsColorsInfos[levelIndex].materialsColors[i].color);
            }
        }

        lastUsedLevelIndex = levelIndex;
    }
}
