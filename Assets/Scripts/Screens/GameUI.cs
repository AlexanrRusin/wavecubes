﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : Screen
{
    [SerializeField] Sprite completedStage, nowStage, futureStage;
    [SerializeField] float stageSize, lineWidth;
    [SerializeField] RectTransform stagesParent;
    [SerializeField] Text coinsCountLabel;
    [SerializeField] Button tapToPlayButton;
    [SerializeField] Text currentLevelLabel;
    [SerializeField] Text nextLevelLabel;
    [SerializeField] Button shopButton;
    [SerializeField] Text stageClearText;

    List<Image> stagesImages;

    void Awake()
    {
        base.Init();

        Stage.OnCoinsCountChanged += Stage_OnCoinsCountChanged;
        Level.OnStageIndexChanged += Level_OnStageIndexChanged;
        Level.OnLevelStateChanged += Level_OnLevelStateChanged;
        StatisticsManager.OnCurrentLevelChanged += StatisticsManager_OnCurrentLevelChanged;
    }


    void OnDestroy()
    {
        Stage.OnCoinsCountChanged -= Stage_OnCoinsCountChanged;
        Level.OnStageIndexChanged -= Level_OnStageIndexChanged;
        Level.OnLevelStateChanged -= Level_OnLevelStateChanged;
        StatisticsManager.OnCurrentLevelChanged -= StatisticsManager_OnCurrentLevelChanged;
    }


    public override void CloseScreen(float delay = 0)
    {
        anim.clip = anim.GetClip("gameUIClose");
        base.CloseScreen(delay);
        ChangeTapToStartButtonActivity(false);
    }

    public override void OpenScreen(float delay = 0)
    {
        base.OpenScreen(delay);
        anim.clip = anim.GetClip("gameUIOpen");

        int currentLevel = StatisticsManager.Instance.CurrentLevel + 1;
        currentLevelLabel.text = currentLevel.ToString();
        nextLevelLabel.text = (currentLevel+1).ToString();
    }

    void SpawnStages(int count)
    {
        foreach (Transform child in stagesParent)
        {
            Destroy(child.gameObject);
        }
        float y = 0;
        float x = -stagesParent.sizeDelta.x / 2, rectX = x;
        float delta = stagesParent.sizeDelta.x / (count + 1);
        float distance = (stagesParent.sizeDelta.x - count * stageSize) / (count + 1);
        x += (distance + stageSize / 2);

        stagesImages = new List<Image>(count);

        for (int i = 0; i < count; i++)
        {
            GameObject NewObj = new GameObject();
            Image NewImage = NewObj.AddComponent<Image>();
            NewImage.sprite = completedStage;
            RectTransform stageRect = NewObj.GetComponent<RectTransform>();
            stageRect.transform.parent = (stagesParent);
            NewObj.SetActive(true);
            stageRect.sizeDelta = new Vector2(stageSize, stageSize);
            stageRect.localScale = new Vector3(1, 1, 1);
            stageRect.anchoredPosition = new Vector2(x, y);
            x += distance + stageSize;

            stagesImages.Add(NewImage);
        }
        rectX += distance / 2;
        for (int i = 0; i < count + 1; i++)
        {
            GameObject NewObj = new GameObject();
            Image NewImage = NewObj.AddComponent<Image>();
            NewImage.sprite = completedStage;
            RectTransform stageRect = NewObj.GetComponent<RectTransform>();
            stageRect.SetParent(stagesParent);
            NewObj.SetActive(true);
            stageRect.sizeDelta = new Vector2(distance, lineWidth);
            stageRect.localScale = new Vector3(1, 1, 1);
            stageRect.anchoredPosition = new Vector2(rectX, y);
            rectX += distance + stageSize;
        }
    }


    public void OpenShop()
    {
        ScreenManager.Instance.GetScreen<ShopScreen>().OpenScreen();
    }


    public void ChangeTapToStartButtonActivity(bool isActive)
    {
        tapToPlayButton.gameObject.SetActive(isActive);
    }


    void Stage_OnCoinsCountChanged(int coinsCount, int maxCoinsCount)
    {
        coinsCountLabel.text = string.Format("{0}/{1}", coinsCount, maxCoinsCount);
    }


    public void TapToPlayButton_OnClick()
    {
        ChangeTapToStartButtonActivity(false);
        LevelsManager.Instance.CurrentLevel.CurrentLevelState = LevelState.RunStage;
    }


    List<string> stageText = new List<string> {"Amazing!", "Stage clear!", "Cool!", "Great job!"};

    void ShowStageClearText()
    {
        stageClearText.text = stageText[Random.Range(0, stageText.Count)];
        stageClearText.gameObject.SetActive(true);
        Scheduler.Instance.CallMethodWithDelay(() =>
        {
            stageClearText.gameObject.SetActive(false);
        }, stageClearText.GetComponent<Animation>().clip.length);
    }

    void Level_OnStageIndexChanged(int currentStageIndex, int stagesCount)
    {
        if (stagesImages == null || stagesImages.Count != stagesCount)
        {
            SpawnStages(stagesCount);
        }

        for (int i = 0; i < stagesCount; i++)
        {
            if (i == currentStageIndex)
            {
                stagesImages[i].sprite = nowStage;
            }
            else if (i > currentStageIndex)
            {
                stagesImages[i].sprite = futureStage;
            }
            else if (i < currentStageIndex)
            {
                stagesImages[i].sprite = completedStage;
            }
        }
    }


    void Level_OnLevelStateChanged(LevelState levelState)
    {
        switch(levelState)
        {
            case LevelState.RunStage:
                shopButton.gameObject.SetActive(false);
                break;
            case LevelState.SpawnStage:
                shopButton.gameObject.SetActive(true);
                break;
            case LevelState.WinStage:
                ShowStageClearText();

                GameObject confettiEffect = EffectsManager.Instance.PlayEffect(EffectType.StageConfetti, 1.5f, Vector3.zero);
                confettiEffect.transform.parent = Camera.main.transform;
                confettiEffect.transform.Rotate(Vector3.zero, Space.World);

                confettiEffect.transform.position = Camera.main.transform.position + new Vector3(0, -1.5f, 2.5f);

                break;
        }
    }


    void StatisticsManager_OnCurrentLevelChanged(int currentLevelIndex)
    {

    }
}
