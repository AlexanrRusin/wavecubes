﻿using UnityEngine;
using UnityEngine.UI;

public class ShopScreen : Screen
{
    [SerializeField] Text moneyLabel;
    [SerializeField] Button addCoinsForVideoButton;

    void Awake()
    {
        base.Init();

        StatisticsManager.OnCoinsCountChanged += StatisticsManager_OnCoinsCountChanged;
        addCoinsForVideoButton.onClick.AddListener(AddCoinsForVideoButton_OnClick);
    }


    private void OnDestroy()
    {
        StatisticsManager.OnCoinsCountChanged -= StatisticsManager_OnCoinsCountChanged;
        addCoinsForVideoButton.onClick.RemoveListener(AddCoinsForVideoButton_OnClick);
    }


    public override void CloseScreen(float delay = 0)
    {
        anim.clip = anim.GetClip("shopClose");
        base.CloseScreen(delay);
    }

    public override void OpenScreen(float delay = 0)
    {
        base.OpenScreen(delay);
        anim.clip = anim.GetClip("shopOpen");
        moneyLabel.text = StatisticsManager.Instance.CoinsCount.ToString();
    }

    public void CloseShop()
    {
        ScreenManager.Instance.previousScreen.OpenScreen();
    }


    void StatisticsManager_OnCoinsCountChanged(int coinsCount)
    {
        moneyLabel.text = coinsCount.ToString();
    }


    void AddCoinsForVideoButton_OnClick()
    {
        AdvertisingManager.Instance.TryShowRewardVideo((resultType) =>
        {
            if (resultType == AdResult.Success)
            {
                StatisticsManager.Instance.CoinsCount += 1000;
            }
        });
    }
}
