﻿

using UnityEngine;
using UnityEngine.UI;

public class WinScreen : Screen
{
    [SerializeField] Button noThanksBtn;
    [SerializeField] Button claimX5Button;
    [SerializeField] GameObject noThanks;
    [SerializeField] Animation noThanksAnim, casino, multiplierAnim;
    [SerializeField] float timeToShowNoThanks;
    [SerializeField] Text money, multiplier, levelPassedText;
    int multiplierNumber;


    private void Awake()
    {
        base.Init();
        noThanksBtn.onClick.AddListener(TapToContinue_OnClick);
        claimX5Button.onClick.AddListener(ClaimX5Button_OnClick);
    }


    private void OnDestroy()
    {
        noThanksBtn.onClick.RemoveListener(TapToContinue_OnClick);
        claimX5Button.onClick.RemoveListener(ClaimX5Button_OnClick);
    }


    public override void CloseScreen(float delay = 0)
    {
        anim.clip = anim.GetClip("winScreenClose");
        base.CloseScreen(delay);
        noThanksBtn.interactable = false;
        noThanks.SetActive(false);
        multiplierAnim.Stop();
        multiplier.gameObject.SetActive(false);
    }

    public override void OpenScreen(float delay = 0)
    {
        base.OpenScreen(delay);
        anim.clip = anim.GetClip("winScreenOpen");
        noThanksBtn.interactable = true;
        Scheduler.Instance.CallMethodWithDelay(() => { 
            noThanks.SetActive(true);
            noThanksAnim.Play();
        }, timeToShowNoThanks);
        string casinoAnimName = "";
        multiplierNumber = GetMultiplier();
        switch (multiplierNumber)
        {
            case 1: casinoAnimName = "x1Casino";
                break;
            case 2:
                casinoAnimName = "x2Casino";
                break;
            case 3:
                casinoAnimName = "x3Casino";
                break;
        }
        casino.Play(casinoAnimName);
        Scheduler.Instance.CallMethodWithDelay(() => {
            multiplier.gameObject.SetActive(true);
            multiplier.text = "x"+multiplierNumber;
            multiplierAnim.Play();
        }, casino[casinoAnimName].length);

        levelPassedText.text = "Level " + (StatisticsManager.Instance.CurrentLevel) + " passed!";

        money.text = CoinsCountToAdd().ToString();
    }

    int GetMultiplier()
    {
        int x1 = 55, x2 = 30, x3 = 15;
        int value = Random.Range(0,x1+x2+x3);
        if (value < x1) 
        {
            return 1;
        }
        else if (value < x1+x2)
        {
            return 2;
        }
        else 
        {
            return 3;
        }
    }


    int CoinsCountToAdd()
    {
        int coinsCountToAdd = 0;
        for (int i = 0; i < LevelsManager.Instance.CurrentLevel.CoinsCountAtStages.Count; i++)
        {
            coinsCountToAdd += LevelsManager.Instance.CurrentLevel.CoinsCountAtStages[i];
        }

        return coinsCountToAdd;
    }


    void StartNewLevel()
    {
        LevelsManager.Instance.SetLevel(StatisticsManager.Instance.CurrentLevel);
        LevelsManager.Instance.CurrentLevel.CurrentLevelState = LevelState.SpawnStage;
        ScreenManager.Instance.GetScreen<GameUI>().OpenScreen();
    }


    void TapToContinue_OnClick()
    {
        StatisticsManager.Instance.CoinsCount += CoinsCountToAdd() * multiplierNumber;
        AdvertisingManager.Instance.TryShowInterstitial(null);

        StartNewLevel();
    }


    void ClaimX5Button_OnClick()
    {
        AdvertisingManager.Instance.TryShowRewardVideo((resultType) =>
        {
            if (resultType == AdResult.Success)
            {
                StatisticsManager.Instance.CoinsCount += CoinsCountToAdd() * multiplierNumber * 5;
                StartNewLevel();
            }
        });
    }
}
