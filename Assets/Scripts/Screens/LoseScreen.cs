﻿

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoseScreen : Screen
{
    [SerializeField] Button restartBtn, reviveBtn, squareReviveBtn;
    [SerializeField] Animation roundCounterAnim, noThanksAnim;
    [SerializeField] GameObject playImage;
    [SerializeField] Text counterText;

    void Awake()
    {
        base.Init();

        restartBtn.onClick.AddListener(RestartButton_OnClick);
        reviveBtn.onClick.AddListener(ReviveButton_OnClick);
        squareReviveBtn.onClick.AddListener(ReviveButton_OnClick);
    }


    void OnDestroy()
    {
        restartBtn.onClick.RemoveListener(RestartButton_OnClick);
        reviveBtn.onClick.RemoveListener(ReviveButton_OnClick);
        squareReviveBtn.onClick.RemoveListener(ReviveButton_OnClick);
    }


    public override void CloseScreen(float delay = 0)
    {
        anim.clip = anim.GetClip("loseScreenClose");
        base.CloseScreen(delay);
        restartBtn.interactable = false;
        reviveBtn.interactable = false;
        playImage.SetActive(false);
    }


    public override void OpenScreen(float delay = 0)
    {
        base.OpenScreen(delay);
        anim.clip = anim.GetClip("loseScreenOpen");
        restartBtn.interactable = true;
        reviveBtn.interactable = true;
        roundCounterAnim.Play();
        StartCoroutine(Counter());
    }


    IEnumerator Counter()
    {
        for(int i = 5; i > 0; i--)
        {
            counterText.text = i.ToString();
            yield return new WaitForSeconds(1f);
            if (i == 3)
            {
                noThanksAnim.Play();
            }
        }
        playImage.SetActive(true);
    }


    void RestartButton_OnClick()
    {
        AdvertisingManager.Instance.TryShowInterstitial(null);
        LevelsManager.Instance.CurrentLevel.CurrentLevelState = LevelState.ResetStage;
    }


    void ReviveButton_OnClick()
    {
        AdvertisingManager.Instance.TryShowRewardVideo((resultType) =>
        {
            if (resultType == AdResult.Success)
            {
                LevelsManager.Instance.CurrentLevel.CurrentLevelState = LevelState.ReviveStage;
            }
        });
    }
}
