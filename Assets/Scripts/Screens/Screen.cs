﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Screen : MonoBehaviour
{
    public static event Action<Screen> OnScreenClosed;
    public static event Action<Screen> OnScreenOpen;

    public Animation anim;
    public bool isVisible;


    virtual public void Init()
    {
        anim = GetComponent<Animation>();
    }


    virtual public void OpenScreen(float delay = 0)
    {
        if(this != ScreenManager.Instance.currentScreen)
        {
            isVisible = true;
            ScreenManager.Instance.CloseCurrentScreen();
            ScreenManager.Instance.currentScreen = this;
            if (System.Math.Abs(delay) < Mathf.Epsilon)
            {
                gameObject.SetActive(true);
                Scheduler.Instance.CallMethodWithDelay(() =>
                {
                    anim.Play();
                }, 0f);
            }
            else
            {
                Debug.Log("else");
                Scheduler.Instance.CallMethodWithDelay(() =>
                {
                    gameObject.SetActive(true);
                    if (anim != null)
                    {
                        anim.Play();
                    }
                }, delay);
            }

            OnScreenOpen?.Invoke(this);
        }
    }
    virtual public void CloseScreen(float delay = 0)
    {
        if(this == ScreenManager.Instance.currentScreen)
        {
            isVisible = false;
            ScreenManager.Instance.currentScreen = null;
            ScreenManager.Instance.previousScreen = this;
            if (anim != null)
            {
                Scheduler.Instance.CallMethodWithDelay(() =>
                {
                    anim.Play();
                }, delay);
                delay += anim.clip.length;
            }
            if (System.Math.Abs(delay) < Mathf.Epsilon)
            {
                gameObject.SetActive(false);
            }
            else
            {
                Scheduler.Instance.CallMethodWithDelay(() =>
                {
                    gameObject.SetActive(false);
                }, delay);
            }

            OnScreenClosed?.Invoke(this);
        }
    }
}
