﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraController : Singleton<CameraController>
{
    [SerializeField] Camera mainCamera;
    [SerializeField] Vector3 targetOffset;

    Transform targetToFollow;
    Transform emptyTransformToFollow;

    public Transform TargetToFollow
    {
        get
        {
            if (targetToFollow == null)
            {
                targetToFollow = EmptyTransformToFollow;
            }

            return targetToFollow;
        }
        set
        {
            targetToFollow = value;
        }
    }


    public Transform EmptyTransformToFollow
    {
        get
        {
            if (emptyTransformToFollow == null)
            {
                emptyTransformToFollow = new GameObject("emptyTransformToFollow").transform;
                emptyTransformToFollow.SetParent(transform);
            }

            return emptyTransformToFollow;
        }
    }


    void Update()
    {
        if (TargetToFollow)
        {
            mainCamera.transform.position = new Vector3(0, 0, TargetToFollow.position.z) + targetOffset;
        }
    }
}
