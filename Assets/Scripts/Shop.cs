﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [SerializeField]
    GameObject shopSquare;

    [SerializeField]
    Item[] items;

    GameManager game;

    [SerializeField]
    Font textFont;

    [SerializeField]
    float cellSizeX, cellSizeY, imageSizeX, imageSizeY,textSizeX,textSizeY;

    [SerializeField]
    int rows, columns;

    [SerializeField]
    Sprite selected, backImage,coinSprite;

    [SerializeField]
    Color ownedColor, unOwnedColor, canBuy, cantBuy, selectedColor;

    [SerializeField]
    Animation bottomBtnAnim;

    [SerializeField]
    Button buyBtn;

    bool isShopVisible;
    bool isBuyBtnVisible;
    bool[] owned;
    int lastCube;
    List<GameObject> coins = new List<GameObject>();
    List<Image> images = new List<Image>();
    GameObject selectedRing;
    int selectedItem;
    [System.Serializable]
    public class Item 
    {
        public string tag = "";
        public int price = 0;
        public Sprite itemImage;
        public bool owned;
        public bool isEquiped;
        public SkinType skin;
    }


    void Awake()
    {
        StatisticsManager.OnCoinsCountChanged += StatisticsManager_OnCoinsCountChanged;
    }


    void OnDestroy()
    {
        StatisticsManager.OnCoinsCountChanged -= StatisticsManager_OnCoinsCountChanged;
    }


    public void ChooseItem(int itemNumber)
    {
        SetChoosedRing(itemNumber);
        if (owned[itemNumber]) {
            HideBuyBtn();
            lastCube = itemNumber;
            PlayerPrefs.SetInt("lastCube",lastCube);
            SkinsManager.Instance.CurrenSkinType = items[itemNumber].skin;
            selectedItem = itemNumber;
        }
        else
        {
            ShowBuyBtn();
            selectedItem = itemNumber;
            buyBtn.interactable = StatisticsManager.Instance.CoinsCount >= items[itemNumber].price;
        }
    }

    void ShowBuyBtn()
    {
        if (!isBuyBtnVisible)
        {
            isBuyBtnVisible = true;
            bottomBtnAnim.Play("showBuyButton");
        }
    }

    void HideBuyBtn()
    {
        if (isBuyBtnVisible)
        {
            isBuyBtnVisible = false;
            bottomBtnAnim.Play("hideBuyButton");
        }
    }

    void SetChoosedRing(int itemNumber)
    {
        if (selectedRing != null)
        {
            Destroy(selectedRing);
        }

        Transform cell = shopSquare.transform.GetChild(itemNumber);

        Rect selectedRect = CreateRect(new Vector2(0,0),new Vector2(cellSizeX,cellSizeY));

        selectedRing = CreateImage(cell.GetComponent<RectTransform>(), selectedColor, selected, selectedRect);
    }

    public void BuyItem()
    {
        StatisticsManager.Instance.CoinsCount -= items[selectedItem].price;

        SkinsManager.Instance.CurrenSkinType = items[selectedItem].skin;

        owned[selectedItem] = true;
        string ownedString = JsonHelper.ToJson(owned);
        PlayerPrefs.SetString("owned", ownedString);

        HideBuyBtn();
        UpdateShop();
    }

    public void EquipItem(int itemNumber)
    {

    }

    public void UpdateShop() 
    {
        if (shopSquare.transform.childCount == 0)
        {
            CreateShopItems();
        }
        for(int i = 0; i < items.Length; i++)
        {
            if (!owned[i])
            {
                SetUnOwned(i);
                //if (true/*items[i].price > game.money*/)
                //{
                //    SetEnabled(i);
                //}
                //else
                //{
                //    SetEnabled(i);
                //}
            }
            else
            {
                SetOwned(i);
            }
        }
    }

    void SetDisabled(int itemNumber)
    {
        Transform cell = shopSquare.transform.GetChild(itemNumber);
        cell.GetComponent<Button>().interactable = false;
        cell.GetComponent<Image>().color = cantBuy;
        //cell.GetComponentInChildren<Image>().color = new Color(1, 1, 1, 0.5f);
    }

    void SetEnabled(int itemNumber)
    {
        Transform cell = shopSquare.transform.GetChild(itemNumber);
        cell.GetComponent<Button>().interactable = true;
        cell.GetComponent<Image>().color = canBuy;
    }

    private void CreateShopItems()
    {
        game = Camera.main.GetComponent<GameManager>();

        GetDataFromPrefs();

        RectTransform shopRect = shopSquare.GetComponent<RectTransform>();
        int itemNumber = 0;
        float shopHeight = shopRect.rect.height;
        float distanceY = (shopHeight - cellSizeY * rows) / (rows+1);
        float y = shopHeight / 2;
        y -= distanceY + cellSizeY / 2;
        for(int i = 0;i<rows;i++)
        {
            FillTheRow(y ,ref itemNumber);
            y -= distanceY + cellSizeY;
        }
    }

    void GetDataFromPrefs()
    {
        lastCube = PlayerPrefs.GetInt("lastCube",0);
        if (!PlayerPrefs.HasKey("owned"))
        {
            owned = new bool[items.Length];
            owned[0] = true;
            for(int i = 1; i < owned.Length; i++)
            {
                owned[i] = false;
            }
            string jsonString = JsonHelper.ToJson(owned);
            PlayerPrefs.SetString("owned",jsonString);
        }
        string ownedString = PlayerPrefs.GetString("owned");
        owned = JsonHelper.FromJson<bool>(ownedString);
    }

    private void FillTheRow(float y, ref int itemNumber) {
        RectTransform shopRect = shopSquare.GetComponent<RectTransform>();
        float shopWidth = shopRect.rect.width;
        float x = -shopWidth / 2, distanceX = (shopWidth - cellSizeX * columns) / (columns + 1);
        x += distanceX + cellSizeX / 2;
        for(int i = 0;i<columns;i++)
        {
            if (itemNumber < items.Length)
            {
                CreateCell(shopRect, x, y, cellSizeX, cellSizeY, itemNumber);
            }
            itemNumber++;
            x += distanceX + cellSizeX;
        }
    }

    private GameObject CreateCell(RectTransform parent,float xPos,float yPos,float xSize,float ySize,int itemNumber)
    {
        Rect cellRect = CreateRect(new Vector2(xPos, yPos),new Vector2(xSize,ySize));
        GameObject NewObj = CreateImage(parent, new Color(1, 1, 1, 0.5f),backImage,cellRect);
        RectTransform rect = NewObj.GetComponent<RectTransform>();
        Button btn = NewObj.AddComponent<Button>();
        btn.onClick.AddListener(() => { ChooseItem(itemNumber); });


        Rect imageRect = CreateRect(new Vector2(0,0 /*(cellSizeY - imageSizeY) / 2*/), new Vector2(imageSizeX, imageSizeY));

        images.Add(CreateImage(rect, Color.white, items[itemNumber].itemImage, imageRect).GetComponent<Image>());

        Rect textRect = CreateRect(new Vector2(0, -cellSizeY/2-textSizeY/2), new Vector2(cellSizeX, textSizeY));
        CreateText(rect, items[itemNumber].price.ToString(),textRect);

        Rect coinRect = CreateRect(new Vector2( cellSizeX/2 - textSizeY/2 + 20, -cellSizeY / 2 - textSizeY / 2 ), new Vector2(textSizeY-25,textSizeY-25));
        coins.Add(CreateImage(rect, Color.white, coinSprite, coinRect));

        return NewObj;
    }

    private void CreateText(RectTransform parent,string textString, Rect rect)
    {
        GameObject newText = new GameObject();
        Text text = newText.AddComponent<Text>();
        text.text = textString;
        RectTransform textRect = newText.GetComponent<RectTransform>();
        textRect.SetParent(parent);
        textRect.anchoredPosition = rect.position;
        textRect.sizeDelta = rect.size;
        text.alignment = TextAnchor.MiddleCenter;
        text.font = textFont;
        text.resizeTextForBestFit = true;
        text.resizeTextMaxSize = 100;
        textRect.localScale = new Vector3(1, 1, 1);
    }

    private GameObject CreateImage(RectTransform parent,Color color ,Sprite sprite, Rect rect)
    {
        GameObject newImage = new GameObject();
        Image image = newImage.AddComponent<Image>();
        image.sprite = sprite;
        RectTransform imageRect = newImage.GetComponent<RectTransform>();
        imageRect.SetParent(parent);
        imageRect.anchoredPosition = rect.position;
        imageRect.sizeDelta = rect.size;
        image.color = color;
        image.useSpriteMesh = true;
        imageRect.localScale = new Vector3(1, 1, 1);
        return newImage;
    }

    Rect CreateRect(Vector2 centerPosition, Vector2 size) { 
        return new Rect
        {
            position = centerPosition,
            size = size
        };
    }

    void ShowEquiped(int itemNumber) 
    {
        GameObject newImage = new GameObject();
        Image image = newImage.AddComponent<Image>();
        image.sprite = selected;
        RectTransform imageRect = newImage.GetComponent<RectTransform>();
        imageRect.SetParent(shopSquare.transform.GetChild(itemNumber));
        imageRect.anchoredPosition = Vector2.zero;
        imageRect.sizeDelta = new Vector2(cellSizeX, cellSizeY);
        imageRect.localScale = new Vector3(1f, 1f, 1f);
    }

    void SetOwned(int itemNumber) 
    {
        Transform cell = shopSquare.transform.GetChild(itemNumber);
        cell.GetComponent<Image>().color = ownedColor;
        cell.GetComponentInChildren<Text>().text = "";
        //cell.GetComponentInChildren<Image>().color = ownedColor;
        images[itemNumber].color = new Color(1,1,1,1);
        coins[itemNumber].SetActive(false);
    }

    void SetUnOwned(int itemNumber)
    {
        Transform cell = shopSquare.transform.GetChild(itemNumber);
        cell.GetComponent<Image>().color = unOwnedColor;
        cell.GetChild(0).GetComponent<Image>().color = unOwnedColor;
    }

    public void HideShop()
    {
        shopSquare.SetActive(false);
    }


    void Start()
    {
        GetDataFromPrefs();
        UpdateShop();
        SetChoosedRing(lastCube);
    }


    void StatisticsManager_OnCoinsCountChanged(int currentCoinsCount)
    {
        buyBtn.interactable = currentCoinsCount >= items[selectedItem].price;
    }
}
