﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;


public enum LevelState
{
    None        = 0,

    SpawnStage  = 1,
    RunStage    = 2,
    WinStage    = 3,
    ResetStage  = 4,
    ReviveStage = 5
}


[CreateAssetMenu(menuName = "ScriptableObjects/Level", fileName = "Level")]
public class Level : ScriptableObject
{
    public static event Action<LevelState> OnLevelStateChanged;
    public static event Action<int, int> OnStageIndexChanged;

    [SerializeField] List<Stage> stagesPrefabs;

    LevelState currentLevelState;
    int currentStageIndex = -1;
    int loseStageIndex = -1;


    int CurrentStageIndex
    {
        get => currentStageIndex;
        set
        {
            if (currentStageIndex != value)
            {
                currentStageIndex = value;
                OnStageIndexChanged?.Invoke(value, stagesPrefabs.Count);
            }
        }
    }

    public List<Stage> StagesPrefabs
    {
        get => stagesPrefabs;
        set
        {
            stagesPrefabs = value;
        }
    }

    public List<int> CoinsCountAtStages { get; private set; }

    public List<Stage> SpawnedStages { get; set; } = new List<Stage>();

    public Vector3 StagePosition { get; set; }

    public Stage CurrentStage { get; private set; }

    public Color WaterLightColor { get; set; }

    public Color WaterDarkColor { get; set; }

    bool ShouldSpawnStage => CurrentStageIndex > loseStageIndex;

    public LevelState CurrentLevelState
    {
        get => currentLevelState;
        set
        {
            if (currentLevelState != value)
            {
                currentLevelState = value;
                OnLevelStateChanged?.Invoke(currentLevelState);

                switch (currentLevelState)
                {
                    case LevelState.SpawnStage:

                        if (ShouldSpawnStage)
                        {
                            SpawnStage();
                        }
                        else
                        {
                            CurrentStage = SpawnedStages[CurrentStageIndex];
                        }

                        CurrentStage.CubesController.transform.position = CurrentStage.CubesControllerInitTransform.position;
                        CurrentStage.CoinsCount = 0;

                        for (int i = 0; i < SpawnedStages.Count; i++)
                        {
                            SpawnedStages[i].gameObject.SetActive(i == CurrentStageIndex || i == CurrentStageIndex - 1);
                        }

                        Scheduler.Instance.CallMethodWithDelay(() =>
                        {
                            LevelsManager.Instance.LevelsColorsSettings.RefreshColors(StatisticsManager.Instance.CurrentLevel);
                        }, 0.4f);

                        MoveCameraToNewStage(() =>
                        {
                            CurrentStage.SpawnCubes(() =>
                            {
                                if (CurrentStageIndex > 0)
                                {
                                    SpawnedStages[CurrentStageIndex - 1].gameObject.SetActive(false);
                                }

                                if (CurrentStageIndex == 0)
                                {
                                    Scheduler.Instance.CallMethodWithDelay(() =>
                                    {
                                        AdvertisingManager.Instance.TryShowBanner((resultType) =>
                                        {
                                            if (resultType == AdResult.Success && ScreenManager.Instance.GetScreen<ShopScreen>().isVisible)
                                            {
                                                AdsIntegrationService.Instance.HideBanner();
                                            }
                                        });
                                    }, 1f);
                                }

                                ScreenManager.Instance.GetScreen<GameUI>().ChangeTapToStartButtonActivity(true);
                            });
                        });

                        break;

                    case LevelState.RunStage:

                        CurrentStage.CurrentStageState = StageState.Run;

                        break;

                    case LevelState.WinStage:

                        CoinsCountAtStages[CurrentStageIndex] = CurrentStage.CoinsCount;

                        CurrentStageIndex++;

                        if (ShouldSpawnStage)
                        {
                            StagePosition = CurrentStage.EndPosition;
                        }

                        if (CurrentStageIndex < stagesPrefabs.Count)
                        {
                            CurrentLevelState = LevelState.SpawnStage;
                        }
                        else
                        {
                            EffectsManager.Instance.PlayEffect(EffectType.LevelConfetti, 1.5f, Camera.main.transform.position + new Vector3(0,-1.5f,2.5f));
                            Scheduler.Instance.CallMethodWithDelay(() => 
                            { 
                                ScreenManager.Instance.GetScreen<WinScreen>().OpenScreen(); 
                            }, 1.5f);
                            StatisticsManager.Instance.CurrentLevel++;
                        }

                        break;

                    case LevelState.ResetStage:

                        if (CurrentStageIndex > loseStageIndex)
                        {
                            loseStageIndex = CurrentStageIndex;
                        }

                        ResetToStage(0);

                        break;

                    case LevelState.ReviveStage:

                        if (CurrentStageIndex > loseStageIndex)
                        {
                            loseStageIndex = CurrentStageIndex;
                        }

                        ResetToStage(CurrentStageIndex);

                        break;
                }
            }
        }
    }


    public void ResetLevel()
    {
        CurrentStageIndex = 0;
        loseStageIndex = -1;
        SpawnedStages = new List<Stage>();
        CurrentLevelState = LevelState.None;
        CoinsCountAtStages = new List<int>(stagesPrefabs.Count);

        for(int i = 0; i < stagesPrefabs.Count; i++)
        {
            CoinsCountAtStages.Add(0);
        }
    }


    void SpawnStage()
    {
        CurrentStage = Instantiate(stagesPrefabs[CurrentStageIndex], StagePosition, Quaternion.identity);
        CurrentStage.SetWaterColor(WaterLightColor, WaterDarkColor);

        SpawnedStages.Add(CurrentStage);
        CurrentStage.Init();
    }


    void ResetToStage(int index)
    {
        CurrentStage.CurrentStageState = StageState.Reset;

        ScreenManager.Instance.GetScreen<GameUI>().OpenScreen();

        CurrentStageIndex = index;

        CurrentStage = SpawnedStages[index];

        for (int i = 0; i < SpawnedStages.Count; i++)
        {
            SpawnedStages[i].gameObject.SetActive(i >= index);
        }

        MoveCameraToNewStage(() =>
        {
            CurrentStage.CubesController.transform.position = CurrentStage.CubesControllerInitTransform.position;
            CurrentStage.SpawnCubes(() =>
            {
                ScreenManager.Instance.GetScreen<GameUI>().ChangeTapToStartButtonActivity(true);
            });
        });
    }


    void MoveCameraToNewStage(Action callBack)
    {
        CameraController.Instance.EmptyTransformToFollow.position = CameraController.Instance.TargetToFollow.position;
        CameraController.Instance.TargetToFollow = CameraController.Instance.EmptyTransformToFollow;
        CameraController.Instance.EmptyTransformToFollow.DOMove(CurrentStage.CubesControllerInitTransform.position, 1f).OnComplete(() =>
        {
            callBack.Invoke();
        });
    }
}
