﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public enum AdResult
{
    Success,
    Closed,
    Failed
}


[CreateAssetMenu(menuName = "ScriptableObjects/AdvertisingManager", fileName = "AdvertisingManager")]
public class AdvertisingManager : ScriptableSingleton<AdvertisingManager>
{
    [Header("Interstitals settings")]
    [SerializeField] float firstInterstitialDelay;
    [SerializeField] float delayBetweenInterstitials;
    [SerializeField] int minLevelToShowInterstitials;
    [Header("Banner settings")]
    [SerializeField] int minLevelToShowBanner;

    DateTime lastTimeAdShow;
    bool isBannerloaded;


    void Awake()
    {
        Screen.OnScreenOpen += Screen_OnScreenOpen;
    }


    void OnDestroy()
    {
        Screen.OnScreenOpen -= Screen_OnScreenOpen;
    }


    public bool IsInterstitialAvailable
    {
        get
        {
            bool result = true;

            result &= Application.internetReachability != NetworkReachability.NotReachable;
            result &= (DateTime.Now - lastTimeAdShow).TotalSeconds > delayBetweenInterstitials;
            result &= StatisticsManager.Instance.CurrentLevel >= minLevelToShowInterstitials;

            return result;
        }
    }


    public bool IsRewardVideoAvailable
    {
        get
        {
            bool result = true;
            result &= Application.internetReachability != NetworkReachability.NotReachable;

            return result;
        }
    }


    public bool IsBannerAvailable
    {
        get
        {
            bool result = true;
            result &= Application.internetReachability != NetworkReachability.NotReachable;
            result &= StatisticsManager.Instance.CurrentLevel >= minLevelToShowBanner;

            return result;
        }
    }


    public void TryShowInterstitial(Action<AdResult> callback)
    {
        #if UNITY_EDITOR
        callback?.Invoke(AdResult.Success);
        #else
        if (IsInterstitialAvailable)
        {
            AdsIntegrationService.Instance.ShowInterstitial((resultType)=>
            {
                if (resultType == AdResult.Success)
                {
                    lastTimeAdShow = DateTime.Now;
                    callback?.Invoke(resultType);
                }
            });
        }
        #endif
    }


    public void TryShowRewardVideo(Action<AdResult> callback)
    {
        #if UNITY_EDITOR
        callback?.Invoke(AdResult.Success);
        #else
        if (IsRewardVideoAvailable)
        {
            AdsIntegrationService.Instance.ShowRewarded((resultType) =>
            {
                if (resultType == AdResult.Success)
                {
                    lastTimeAdShow = DateTime.Now;
                    callback?.Invoke(resultType);
                }
            });
        }
        #endif
    }


    public void TryShowBanner(Action<AdResult> callback)
    {
        #if UNITY_EDITOR
        callback?.Invoke(AdResult.Success);
        #else
        if (IsBannerAvailable && !isBannerloaded)
        {
            AdsIntegrationService.Instance.LoadBanner((resultType) =>
            {
                if (resultType == AdResult.Success)
                {
                    isBannerloaded = true;
                }
                else if (resultType == AdResult.Failed)
                {
                    Scheduler.Instance.CallMethodWithDelay(()=>
                    {
                        TryShowBanner(null);
                    }, 30f);
                }

                callback?.Invoke(resultType);
            });
        }
        #endif
    }


    protected override void Init()
    {
        base.Init();

        lastTimeAdShow = DateTime.Now.AddSeconds(firstInterstitialDelay);
    }


    void Screen_OnScreenOpen(Screen screen)
    {
        if (isBannerloaded)
        {
            if (screen is ShopScreen)
            {
                AdsIntegrationService.Instance.HideBanner();
            }
            else if (screen is GameUI)
            {
                AdsIntegrationService.Instance.ShowBanner();
            }
        }
    }
}
