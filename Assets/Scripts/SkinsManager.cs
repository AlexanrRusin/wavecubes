﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public enum SkinType
{
    None        = 0,

    Default     = 1,

    Silver      = 2,

    Gold        = 3,

    Diamond     = 4,

    Lego        = 5,

    Circle      = 6
}


[Serializable]
public class SkinInfo
{
    public Cube prefab;
    public SkinType skinType;
}


[CreateAssetMenu(menuName = "ScriptableObjects/SkinsManager", fileName = "SkinsManager")]
public class SkinsManager : ScriptableSingleton<SkinsManager>
{
    [SerializeField] List<SkinInfo> skinsInfo;



    public Cube CurrentSkin => GetSkinByType(CurrenSkinType);


    public SkinType CurrenSkinType
    {
        get => (SkinType)PlayerPrefs.GetInt("CurrentSkin", 1);
        set
        {
            PlayerPrefs.SetInt("CurrentSkin", (int)value);
        }
    }


    public Cube GetSkinByType(SkinType skinType)
    {
        return skinsInfo.Find(x => x.skinType == skinType).prefab;
    }
}
