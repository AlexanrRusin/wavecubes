﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum StageState
{
    None        = 0,
    Run         = 1,
    Stop        = 2,
    Win         = 3,
    Lose        = 4,
    Reset       = 5
}


public class Stage : MonoBehaviour
{
    public static event Action<int, int> OnCoinsCountChanged;

    [SerializeField] CubesController cubesController;
    [SerializeField] MeshRenderer waterMeshRenderer;
    [SerializeField] Transform stageEndTransform;
    [SerializeField] Transform cubesControllerInitTransform;

    StageState currentStageState;
    int coinsCount = -1;
    int maxCoinsCount;
    SkinType skinTypeAtStage;

    public Vector3 EndPosition => stageEndTransform.position;

    public CubesController CubesController => cubesController;

    public Transform CubesControllerInitTransform => cubesControllerInitTransform;

    public StageState CurrentStageState
    {
        get => currentStageState;
        set
        {
            if (currentStageState != value)
            {
                currentStageState = value;

                switch(currentStageState)
                {
                    case StageState.Run:
                        CameraController.Instance.TargetToFollow = cubesController.transform;
                        CoinsCount = 0;
                        break;
                    case StageState.Win:
                        LevelsManager.Instance.CurrentLevel.CurrentLevelState = LevelState.WinStage;
                        coinsCount = -1;
                        break;
                    case StageState.Lose:
                        ScreenManager.Instance.GetScreen<LoseScreen>().OpenScreen();
                        break;
                    case StageState.Reset:
                        CubesController.RemoveAllCubes();
                        coinsCount = -1;
                        break;
                }
            }
        }
    }


    public int CoinsCount
    {
        get => coinsCount;
        set
        {
            if (coinsCount != value)
            {
                coinsCount = value;
                OnCoinsCountChanged?.Invoke(coinsCount, maxCoinsCount);
            }
        }
    }


    void Awake()
    {
        CubesController.OnCubeRemoved += CubesController_OnCubeRemoved;
        Screen.OnScreenClosed += Screen_OnScreenClosed;

    }


    void OnDestroy()
    {
        CubesController.OnCubeRemoved -= CubesController_OnCubeRemoved;
        Screen.OnScreenClosed -= Screen_OnScreenClosed;
    }


    void Update()
    {
        switch(CurrentStageState)
        {
            case StageState.Run:
                cubesController.UpdateMovement();
                break;
        }
    }


    public void Init()
    {
        maxCoinsCount = CubesController.MaxCubesCount;
    }


    public void SetWaterColor(Color lightColor, Color darkcolor)
    {
        MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();

        materialPropertyBlock.SetColor("_LightColor", lightColor);
        materialPropertyBlock.SetColor("_newLightColor", lightColor);

        materialPropertyBlock.SetColor("_DarkColor", darkcolor);
        materialPropertyBlock.SetColor("_newDarkColor", darkcolor);

        waterMeshRenderer.SetPropertyBlock(materialPropertyBlock);
    }


    public void SpawnCubes(Action callback)
    {
        cubesController.transform.position = CubesControllerInitTransform.position;
        cubesController.SpawnCubes(callback);
        skinTypeAtStage = SkinsManager.Instance.CurrenSkinType;
    }


    void CubesController_OnCubeRemoved()
    {
        if (LevelsManager.Instance.CurrentLevel.CurrentStage == this)
        {
            CoinsCount++;
        }
    }


    void Screen_OnScreenClosed(Screen closedScreen)
    {
        if (LevelsManager.Instance.CurrentLevel.CurrentStage == this && closedScreen is ShopScreen)
        {
            if (skinTypeAtStage != SkinsManager.Instance.CurrenSkinType)
            {
                CubesController.RemoveAllCubes();
                CubesController.SpawnCubes(() =>
                {
                    ScreenManager.Instance.GetScreen<GameUI>().ChangeTapToStartButtonActivity(true);
                });
                skinTypeAtStage = SkinsManager.Instance.CurrenSkinType;
            }
            else
            {
                ScreenManager.Instance.GetScreen<GameUI>().ChangeTapToStartButtonActivity(true);
            }
        }
    }
}
