﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Cube : MonoBehaviour
{
    [SerializeField] BoxCollider boxCollider;
    [SerializeField] Rigidbody rigidbody;
    bool isRemoving;


    public CubesController CubesController { get; set; }

    public BoxCollider BoxCollider => boxCollider;

    public Rigidbody Rigidbody => rigidbody;


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Obstacle>() != null && !isRemoving)
        {
            isRemoving = true;
            CubesController.RemoveCube(this);
            //AudioManager.Instance.PlaySound(SoundType.DestroyCube);
            EffectsManager.Instance.PlayEffect(EffectType.DestroyCube, 2f, transform.position);
            EffectsManager.Instance.PlayEffect(EffectType.AddingScore, 1f, transform.position);
            Destroy(gameObject);
        }
        else if (collision.gameObject.GetComponent<LoseLine>() != null)
        {
            LevelsManager.Instance.CurrentLevel.CurrentStage.CurrentStageState = StageState.Lose;
        }
    }
}
