﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(WaterMesh))]
public class WaterMeshEditor : Editor
{
    Mesh mesh;


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        int pointsPerLineCount = EditorGUILayout.IntField("pointsPerLineCount", EditorPrefs.GetInt("pointsPerLineCount", 5));
        int pointsPerColumnCount = EditorGUILayout.IntField("pointsPerColumnCount", EditorPrefs.GetInt("pointsPerColumnCount", 5));
        float vertexSize = EditorGUILayout.FloatField("vertexSize", EditorPrefs.GetFloat("vertexSize", 1));

        EditorPrefs.SetInt("pointsPerLineCount", pointsPerLineCount);
        EditorPrefs.SetInt("pointsPerColumnCount", pointsPerColumnCount);
        EditorPrefs.SetFloat("vertexSize", vertexSize);

        Vector3[] vertices = new Vector3[(pointsPerColumnCount - 1) * (pointsPerLineCount - 1) * 6];
        Vector3[] normals = new Vector3[vertices.Length];

        int index = 0;

        for (int i = 0; i < pointsPerColumnCount - 1; i++)
        {
            for (int j = 0; j < pointsPerLineCount - 1; j++)
            {
                vertices[index] = new Vector3((j + 1) * vertexSize, 0, i * vertexSize);
                vertices[index + 1] = new Vector3(j * vertexSize, 0, i * vertexSize);
                vertices[index + 2] = new Vector3(j * vertexSize, 0, (i + 1) * vertexSize);

                Vector3 randomNormal = new Vector3(Random.value, Random.value, Random.value);
                normals[index] = randomNormal;
                normals[index + 1] = randomNormal;
                normals[index + 2] = randomNormal;

                vertices[index + 3] = new Vector3((j + 1) * vertexSize, 0, i * vertexSize);
                vertices[index + 4] = new Vector3(j * vertexSize, 0, (i + 1) * vertexSize);
                vertices[index + 5] = new Vector3((j + 1) * vertexSize, 0, (i + 1) * vertexSize);

                randomNormal = new Vector3(Random.value, Random.value, Random.value);
                normals[index + 3] = randomNormal;
                normals[index + 4] = randomNormal;
                normals[index + 5] = randomNormal;

                index += 6;
            }
        }


        int[] triangles = new int[(pointsPerColumnCount - 1) * (pointsPerLineCount - 1) * 6];
       
        for(int i = 0; i < vertices.Length; i++)
        {
            triangles[i] = i;
        }

        if (GUILayout.Button("Recalculate mesh"))
        {
            mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.normals = normals;

            (target as WaterMesh).GetComponent<MeshFilter>().mesh = mesh;
        }
        if (GUILayout.Button("Save mesh"))
        {
            mesh.SaveMesh("WaterMesh", true, true);

            Transform rootObj = new GameObject("endPosition").transform;
            rootObj.parent = (target as WaterMesh).transform;
            rootObj.position = new Vector3(0f, 0f, (pointsPerColumnCount - 1) * vertexSize);

            (target as WaterMesh).GetType().GetField("endTransform", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).SetValue(target as WaterMesh, rootObj);

            (target as WaterMesh).GetComponent<MeshFilter>().mesh = mesh;
        }
    }
}
#endif