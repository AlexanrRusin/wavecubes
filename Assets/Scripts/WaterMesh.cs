﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WaterMesh : MonoBehaviour
{
    [SerializeField] Transform endTransform;
    [SerializeField] MeshRenderer meshRenderer;


    public MeshRenderer MeshRenderer => meshRenderer;

    public Transform EndTransform => endTransform;

}
