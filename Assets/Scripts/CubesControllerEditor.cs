﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.Experimental.SceneManagement;


[CustomEditor(typeof(CubesController))]

public class CubesControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        bool areCubesShowing = PlayerPrefs.GetInt("areCubesShowing", 1) == 1;

        if (GUILayout.Button(areCubesShowing ? "Hide cubes" : "Show cubes"))
        {
            if (areCubesShowing)
            {
                DestroyCubesInRoot((target as CubesController).transform);
                PlayerPrefs.SetInt("areCubesShowing", 0);
            }
            else
            {
                PlayerPrefs.SetInt("areCubesShowing", 1);
                List<CubeSpawnInfo> cubesSpawnInfos = (List<CubeSpawnInfo>)(target as CubesController).GetType().GetField("cubesSpawnInfos", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(target as CubesController);
                for (int i = 0; i < cubesSpawnInfos.Count; i++)
                {
                    Cube cube = Instantiate(SkinsManager.Instance.CurrentSkin, Vector3.zero, cubesSpawnInfos[i].rotation, (target as CubesController).transform);
                    cube.transform.localScale = cubesSpawnInfos[i].scale;
                    cube.transform.localPosition = cubesSpawnInfos[i].localPosition;
                }
            }
        }

        if (GUILayout.Button("Save Cubes"))
        {
          Cube[] cubesInRoot = (target as CubesController).GetComponentsInChildren<Cube>();

            List<CubeSpawnInfo> cubesSpawnInfos = new List<CubeSpawnInfo>(cubesInRoot.Length);

            for (int i = 0; i < cubesInRoot.Length; i++)
            {
                cubesSpawnInfos.Add(new CubeSpawnInfo
                {
                    localPosition = cubesInRoot[i].transform.localPosition,
                    rotation = cubesInRoot[i].transform.rotation,
                    scale = cubesInRoot[i].transform.localScale
                });
            }

            (target as CubesController).GetType().GetField("cubesSpawnInfos", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).SetValue(target as CubesController, cubesSpawnInfos);

            DestroyCubesInRoot((target as CubesController).transform);

            var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
            if (prefabStage != null)
            {
                EditorSceneManager.MarkSceneDirty(prefabStage.scene);
            }
        }
    }


    void DestroyCubesInRoot(Transform root)
    {
        Cube[] cubesInRoot = root.GetComponentsInChildren<Cube>();

        for (int i = 0; i < cubesInRoot.Length; i++)
        {
            DestroyImmediate(cubesInRoot[i].gameObject);
        }
    }
}
#endif