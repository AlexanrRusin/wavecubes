﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public enum SoundType
{
    None            = 0,
    DestroyCube     = 1
}

[Serializable]
public class SoundInfo
{
    public AudioClip sound;
    public SoundType soundType;
}


public class AudioManager : Singleton<AudioManager>
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] List<SoundInfo> soundsInfo;


    public void PlaySound(SoundType soundType)
    {
        audioSource.clip = soundsInfo.Find(x => x.soundType == soundType).sound;
        audioSource.Play();
    }
}
