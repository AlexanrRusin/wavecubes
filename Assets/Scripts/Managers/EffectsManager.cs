﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public enum EffectType
{
    None            = 0,
    DestroyCube     = 1,
    AddingScore     = 2,
    StageConfetti   = 3,
    LevelConfetti   = 4
}


[Serializable]
public class EffectInfo
{
    public GameObject prefab;
    public EffectType effectType;
}


public class EffectsManager : Singleton<EffectsManager>
{
    [SerializeField] List<EffectInfo> effectsInfos;


    public GameObject PlayEffect(EffectType effectType, float duration, Vector3 position, bool shouldDestroy = true)
    {
        GameObject effectPrefab = effectsInfos.Find(x => x.effectType == effectType).prefab;
        GameObject effect = Instantiate(effectPrefab, transform);
        effect.transform.position = position;

        if (shouldDestroy)
        {
            Scheduler.Instance.CallMethodWithDelay(() =>
            {
                Destroy(effect);
            }, duration);
        }

        return effect;
    }
}
