﻿using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] List<GameObject> preloadedManagers;


    protected override void Awake()
    {
        Application.targetFrameRate = 60;
        FB.Init();
        for(int i = 0; i < preloadedManagers.Count; i++)
        {
            Instantiate(preloadedManagers[i], transform);
        }
    }


    void Start()
    {
        ScreenManager.Instance.GetScreen<GameUI>().ChangeTapToStartButtonActivity(false);

        LevelsManager.Instance.SetLevel(StatisticsManager.Instance.CurrentLevel);
        LevelsManager.Instance.CurrentLevel.CurrentLevelState = LevelState.SpawnStage;
    }
}
