﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "ScriptableObjects/StatisticsManager", fileName = "StatisticsManager")]
public class StatisticsManager : ScriptableSingleton<StatisticsManager>
{
    public static event Action<int> OnCoinsCountChanged;
    public static event Action<int> OnCurrentLevelChanged;


    public int CurrentLevel
    {
        get => PlayerPrefs.GetInt("CurrentLevel");
        set
        {
            if (CurrentLevel != value)
            {
                PlayerPrefs.SetInt("CurrentLevel", value);
                OnCurrentLevelChanged?.Invoke(value);
            }
        }
    }


    public int CoinsCount
    {
        get => PlayerPrefs.GetInt("CoinsCount");
        set
        {
            if (CoinsCount != value)
            {
                PlayerPrefs.SetInt("CoinsCount", value);
                OnCoinsCountChanged?.Invoke(value);
            }
        }
    }
}
