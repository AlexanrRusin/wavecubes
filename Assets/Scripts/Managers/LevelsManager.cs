﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "ScriptableObjects/LevelsManager", fileName = "LevelsManager")]
public class LevelsManager : ScriptableSingleton<LevelsManager>
{
    [SerializeField] List<Level> levels;
    [SerializeField] WaterMesh waterMeshBetweenLevels;
    [SerializeField] LevelsColorsSettings levelsColorSettings;

    Level previousLevel;
    WaterMesh waterMesh;


    public Level CurrentLevel { get; private set; }

    public LevelsColorsSettings LevelsColorsSettings => levelsColorSettings;


    void Awake()
    {
        Level.OnLevelStateChanged += Level_OnLevelStateChanged;
    }


    void OnDestroy()
    {
        Level.OnLevelStateChanged -= Level_OnLevelStateChanged;
    }


    public void SetLevel(int index)
    {
        bool isFirstLevelInSession = CurrentLevel == null;

        Color oldLightColor = Color.white;
        Color oldDarkColor = Color.white;

        if (!isFirstLevelInSession)
        {
            oldLightColor = levelsColorSettings.GetColorsInfo(index - 1).waterLightColor;
            oldDarkColor = levelsColorSettings.GetColorsInfo(index - 1).waterDarkColor;
            previousLevel = CurrentLevel;
        }
        else
        {
            LevelsColorsSettings.RefreshColors(index);
        }

        if (index > levels.Count - 1)
        {
            AddLevel(index);
        }

        CurrentLevel = levels[index];

        CurrentLevel.WaterLightColor = levelsColorSettings.GetColorsInfo(index).waterLightColor;
        CurrentLevel.WaterDarkColor = levelsColorSettings.GetColorsInfo(index).waterDarkColor;

        if (index > 0 && !isFirstLevelInSession)
        {
            if (waterMesh != null)
            {
                Destroy(waterMesh.gameObject);
            }

            waterMesh = Instantiate(waterMeshBetweenLevels, levels[index - 1].CurrentStage.EndPosition, Quaternion.identity);
            CurrentLevel.StagePosition = waterMesh.EndTransform.position;

            SetWaterMeshColorsBetweenLevels(waterMesh, oldLightColor, oldDarkColor, CurrentLevel.WaterLightColor, CurrentLevel.WaterDarkColor);
        }

        CurrentLevel.ResetLevel();
    }


    void SetWaterMeshColorsBetweenLevels(WaterMesh waterMesh, Color oldLightColor, Color oldDarkColor,
                                                              Color newLightColor, Color newDarkColor)
    {
        MaterialPropertyBlock materialPropertyBlock = new MaterialPropertyBlock();

        materialPropertyBlock.SetColor("_LightColor", oldLightColor);
        materialPropertyBlock.SetColor("_DarkColor", oldDarkColor);

        materialPropertyBlock.SetColor("_newLightColor", newLightColor);
        materialPropertyBlock.SetColor("_newDarkColor", newDarkColor);

        materialPropertyBlock.SetFloat("_zSize", waterMesh.EndTransform.localPosition.z);

        waterMesh.MeshRenderer.SetPropertyBlock(materialPropertyBlock);
    }


    int idx;
    private void AddLevel(int index)
    {
        for(int i = levels.Count; i < index; i++)
        {
            levels.Add(levels[idx]); 
        }

        levels[idx].StagesPrefabs = new List<Stage>(4);

        for (int i = 0; i < 4; i++)
        {
            int randLevel = Random.Range(0, levels.Count);

            while (i > levels[randLevel].StagesPrefabs.Count - 1)
            {
                randLevel = Random.Range(0, levels.Count);
            }

            levels[idx].StagesPrefabs.Add(levels[randLevel].StagesPrefabs[i]);
        }

        levels.Add(levels[idx]);
        idx++;
    }


    void Level_OnLevelStateChanged(LevelState levelState)
    {
        if (levelState == LevelState.RunStage && previousLevel != null)
        {
            for(int i = 0; i < previousLevel.SpawnedStages.Count; i++)
            {
                Destroy(previousLevel.SpawnedStages[i].gameObject);
            }

            previousLevel.SpawnedStages = new List<Stage>();
        }
    }
}
