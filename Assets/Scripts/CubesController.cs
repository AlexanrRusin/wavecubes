﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using MoreMountains.NiceVibrations;

[Serializable]
public class CubeSpawnInfo
{
    public Vector3 localPosition;
    public Quaternion rotation;
    public Vector3 scale;
}


public class CubesController : MonoBehaviour
{
    public static event Action OnCubeRemoved;

    [SerializeField] float zSpeed;
    [SerializeField] float xSpeed;
    [SerializeField] List<CubeSpawnInfo> cubesSpawnInfos;
    [SerializeField] AnimationCurve explodeEffectCurve;

    Camera cachedCamera;
    Vector3 previousDirection;

    List<Cube> cubes;

    Transform left, right;


    Camera CachedCamera
    {
        get  
        {
            if (cachedCamera == null)
            {
                cachedCamera = Camera.main;
            }

            return cachedCamera;
        }
    }


    public int MaxCubesCount => cubesSpawnInfos.Count;


    public void UpdateMovement()
    {
        Vector3 currentDirection = Vector3.zero;

        if(cubes.Count != 0)
        {
            FindExtremes();
        }

        if (Input.GetMouseButton(0))
        {
            currentDirection.x = CachedCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -10)).x;
        }

        if (Input.GetMouseButtonDown(0))
        {
            previousDirection.x = currentDirection.x;
        }

        if (Input.GetMouseButtonUp(0))
        {
            previousDirection.x = currentDirection.x;
        }

        transform.position += new Vector3((previousDirection - currentDirection).x * xSpeed * Time.deltaTime, 0, zSpeed * Time.deltaTime);

        transform.position = new Vector3(Mathf.Clamp(transform.position.x,-5f - left.localPosition.x + 0.25f, 5f - right.localPosition.x - 0.25f),transform.position.y,transform.position.z);
        //Mathf.Clamp(transform.position.x + touch.deltaPosition.x * 0.007f, -5 - left.localPosition.x + 0.375f, 5 - 0.375f - right.localPosition.x)
        TryRefreshPreviousDirectionX(currentDirection);

        #if UNITY_EDITOR
        if (Input.GetMouseButtonDown(1))
        {
            RemoveAllCubes();
            LevelsManager.Instance.CurrentLevel.CurrentStage.CurrentStageState = StageState.Win;
        }
        #endif
    }

    void FindExtremes()
    {
        if (cubes.Count == 1)
        {
            left = right = transform.GetChild(0);
        }
        else
        {
            if (cubes.Count > 0)
            {
                left = right = transform.GetChild(0);
                foreach (Cube cube in cubes)
                {
                    if (cube.transform.position.x < left.position.x)
                    {
                        left = cube.transform;
                    }
                    if (cube.transform.position.x > right.position.x)
                    {
                        right = cube.transform;
                    }
                }
            }
        }
    }


    public void SpawnCubes(Action callback)
    {
        cubes = new List<Cube>(cubesSpawnInfos.Count);

        Sequence sequence = DOTween.Sequence();

        Cube currenSkinPrefab = SkinsManager.Instance.CurrentSkin;
        for (int i = 0; i < cubesSpawnInfos.Count; i++)
        {
            Cube cube = Instantiate(currenSkinPrefab, Vector3.zero, cubesSpawnInfos[i].rotation, transform);
            cube.transform.localScale = cubesSpawnInfos[i].scale;
            float x = UnityEngine.Random.Range(5f, 20f)* (1 - UnityEngine.Random.Range(0,2)*2);
            float y = UnityEngine.Random.Range(5f, 20f);
            float z = UnityEngine.Random.Range(5f, 20f) * (1 - UnityEngine.Random.Range(0,2) * 2);
            cube.transform.localPosition = new Vector3(x,y,z);

            cube.Rigidbody.useGravity = false;
            cube.BoxCollider.isTrigger = true;

            float time = UnityEngine.Random.Range(1f, 2f);
            cube.transform.Rotate(new Vector3(UnityEngine.Random.Range(0, 360), UnityEngine.Random.Range(0, 360), UnityEngine.Random.Range(0, 360)));
            sequence.Join(cube.transform.DOLocalMove(cubesSpawnInfos[i].localPosition, time).SetEase(explodeEffectCurve).OnComplete(() =>
            {
                VibrationManager.Instance.PlayVibration(HapticTypes.Selection);
            }));
             
            sequence.Join(cube.transform.DORotate(Vector3.zero, time));

            cube.CubesController = this;
            cubes.Add(cube);
        }

        sequence.OnComplete(() =>
        {
            for (int i = 0; i < cubes.Count; i++)
            {
                cubes[i].Rigidbody.useGravity = true;
                cubes[i].BoxCollider.isTrigger = false;
            }

            VibrationManager.Instance.PlayVibration(HapticTypes.HeavyImpact);

            callback.Invoke();
        });
    }


    public void RemoveCube(Cube cube)
    {
        cubes.Remove(cube);

        OnCubeRemoved?.Invoke();

        VibrationManager.Instance.PlayVibration(HapticTypes.Selection);

        if (cubes.Count == 0)
        {
            LevelsManager.Instance.CurrentLevel.CurrentStage.CurrentStageState = StageState.Win;
        }
    }


    public void RemoveAllCubes()
    {
        for(int i = 0; i < cubes.Count; i++)
        {
            Destroy(cubes[i].gameObject);
        }
    }


    void TryRefreshPreviousDirectionX(Vector3 currentDirection)
    {
        if  (Mathf.Abs((transform.position - previousDirection).magnitude) > 0.1)
        {
            previousDirection = currentDirection;
        }
    }

}
