﻿using System.ComponentModel;
using UnityEngine;

public delegate void SROptionsPropertyChanged(object sender, string propertyName);

public partial class SROptions
{
    private static readonly SROptions _current = new SROptions();

    public static SROptions Current
    {
        get { return _current; }
    }

    public event SROptionsPropertyChanged PropertyChanged;
    #if UNITY_EDITOR
    [JetBrains.Annotations.NotifyPropertyChangedInvocator]
    #endif
    public void OnPropertyChanged(string propertyName)
    {
        if (PropertyChanged != null)
        {
            PropertyChanged(this, propertyName);
        }
    }


    private int _levelInt = 1;
    [Category("Levels navigation")]
    public int Level
    {
        get { return _levelInt; }
        set
        {
            if (value <= 0)
            {
                _levelInt = 1;
            }

            _levelInt = value;

        }
    }


    [Category("Levels navigation")]
    public void GoToLevel()
    {
        StatisticsManager.Instance.CurrentLevel = Level - 1;

        (LevelsManager.Instance).GetType().GetProperty("CurrentLevel", 
                                            System.Reflection.BindingFlags.Instance |
                                            System.Reflection.BindingFlags.NonPublic |
                                            System.Reflection.BindingFlags.Public).SetValue(LevelsManager.Instance, null);

         
        Application.LoadLevel(Application.loadedLevel);
    }
}
