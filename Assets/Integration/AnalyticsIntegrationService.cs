﻿using System.Collections.Generic;
using UnityEngine;

public class AnalyticsIntegrationService : MonoBehaviour
{
    private const string appMetricaApiKey = "63875c3d-264f-493c-bfff-9a4d738d7276"; //Change me つ ◕_◕ ༽つ
    private const string appsFlyerApiKey = "6eRCe9pE9pB7QAkSXDptTa"; //Change me つ ◕_◕ ༽つ
    private const string iosAppleId = "1466086021"; //Change me つ ◕_◕ ༽つ
    private const string adnroidPackageName = "com.harmonybit.unitytemplate"; //Change me つ ◕_◕ ༽つ

    private void Awake()
    {
        InitAppsFlyer();
        InitAppMetrica();
    }

    private void InitAppMetrica()
    {
        AppMetrica.Instance.ActivateWithConfiguration(new YandexAppMetricaConfig(appMetricaApiKey));
        AppMetrica.Instance.ReportEvent("Open_Game", new Dictionary<string, object>() {{"coins", 0}, {"level", 0}}); //TODO: It's a test event
    }

    private void InitAppsFlyer()
    {
        AppsFlyer.setAppsFlyerKey(appsFlyerApiKey);
        /* For detailed logging */
        /* AppsFlyer.setIsDebug (true); */
#if UNITY_IOS
        /* Mandatory - set your apple app ID
           NOTE: You should enter the number only and not the "ID" prefix */
        AppsFlyer.setAppID (iosAppleId);
        AppsFlyer.trackAppLaunch ();
#elif UNITY_ANDROID
        /* Mandatory - set your Android package name */
        AppsFlyer.setAppID(adnroidPackageName);
        /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
        AppsFlyer.init(appsFlyerApiKey, "AppsFlyerTrackerCallbacks");
#endif
    }
}
