﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class AdsIntegrationService : Singleton<AdsIntegrationService>
{
    private const string ironSourceIosApiKey = "9d5ae30d";  
    private const string ironSourceAndroidApiKey = "9db08275";  
    
    //[SerializeField] 
    //Button _showRewardedButton;
    
    //[SerializeField] 
    //Button _showInterstitialButton;

    //[SerializeField] 
    //Text _isRewardedAvailable;
    
    //[SerializeField] 
    //Text _lastPlacement;
    
    //[SerializeField] 
    //Dropdown _bannerSize;

    //[SerializeField] 
    //Dropdown _bannerPosition;

    Action<AdResult> interstitialCallback;
    Action<AdResult> rewardVideoCallback;
    Action<AdResult> bannerCallback;


    protected override void Awake()
    {
        base.Awake();

        InitAds();
    }

    private void InitAds()
    {
        #if UNITY_IOS
        IronSource.Agent.init (ironSourceIosApiKey, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.BANNER, IronSourceAdUnits.INTERSTITIAL);
        #elif UNITY_ANDROID
        IronSource.Agent.init (ironSourceAndroidApiKey, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.BANNER, IronSourceAdUnits.INTERSTITIAL);
        #endif
        IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;

        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += IronSourceEvents_OnRewardedVideoAdShowFailedEvent;

        IronSourceEvents.onBannerAdLoadedEvent += IronSourceEvents_OnBannerAdLoadedEvent;
        IronSourceEvents.onBannerAdLoadFailedEvent += IronSourceEvents_OnBannerAdLoadFailedEvent;


        //PopulateBannerSizes();
        IronSource.Agent.validateIntegration();
        IronSource.Agent.loadInterstitial();
    }

    void IronSourceEvents_OnBannerAdLoadFailedEvent(IronSourceError obj)
    {
        bannerCallback?.Invoke(AdResult.Failed);
    }


    void IronSourceEvents_OnBannerAdLoadedEvent()
    {
        bannerCallback?.Invoke(AdResult.Success);
        ShowBanner();
    }


    void IronSourceEvents_OnRewardedVideoAdShowFailedEvent(IronSourceError obj)
    {
        rewardVideoCallback?.Invoke(AdResult.Failed);
    }

    private void InterstitialAdClosedEvent()
    {
        //_showInterstitialButton.interactable = false;
        interstitialCallback?.Invoke(AdResult.Success);
        IronSource.Agent.loadInterstitial();
    }

    private void InterstitialAdReadyEvent()
    {
        //_showInterstitialButton.interactable = true;
    }

    private void RewardedVideoAdRewardedEvent(IronSourcePlacement obj)
    {
        rewardVideoCallback?.Invoke(AdResult.Success);
        //_lastPlacement.text = obj.ToString();
    }

    //private void PopulateBannerSizes()
    //{
    //    _bannerSize.options = new List<Dropdown.OptionData>
    //    {
    //        new Dropdown.OptionData(IronSourceBannerSize.BANNER.Description),
    //        new Dropdown.OptionData(IronSourceBannerSize.LARGE.Description),
    //        new Dropdown.OptionData(IronSourceBannerSize.SMART.Description),
    //        new Dropdown.OptionData(IronSourceBannerSize.RECTANGLE.Description)
    //    };
        
    //    _bannerPosition.options = new List<Dropdown.OptionData>
    //    {
    //        new Dropdown.OptionData(IronSourceBannerPosition.TOP.ToString()),
    //        new Dropdown.OptionData(IronSourceBannerPosition.BOTTOM.ToString())
    //    };
    //}

    private void RewardedVideoAvailabilityChangedEvent(bool isAvailable)
    {
        //_isRewardedAvailable.text = isAvailable.ToString();
        //_showRewardedButton.interactable = isAvailable;
    }

    public void ShowRewarded(Action<AdResult> callback)
    {
        rewardVideoCallback = callback;
        IronSource.Agent.showRewardedVideo();
    }

    public void ShowInterstitial(Action<AdResult> callback)
    {
        interstitialCallback = callback;
        IronSource.Agent.showInterstitial();
    }

    public void LoadBanner(Action<AdResult> callback)
    {
        bannerCallback = callback;
        Dictionary<string, IronSourceBannerSize> textToSize = new Dictionary<string, IronSourceBannerSize>()
        {
            {"BANNER", IronSourceBannerSize.BANNER},
            {"LARGE", IronSourceBannerSize.LARGE},
            {"SMART", IronSourceBannerSize.SMART},
            {"RECTANGLE", IronSourceBannerSize.RECTANGLE},
        };
        
        Dictionary<string, IronSourceBannerPosition> textToPosition = new Dictionary<string, IronSourceBannerPosition>()
        {
            {"TOP", IronSourceBannerPosition.TOP},
            {"BOTTOM", IronSourceBannerPosition.BOTTOM}
        };

        var size = textToSize["BANNER"];
        var position = textToPosition["BOTTOM"];
        IronSource.Agent.loadBanner(size, position);
    }

    public void ShowBanner()
    {
        IronSource.Agent.displayBanner();
    }
    public void HideBanner()
    {
        IronSource.Agent.hideBanner();
    }
    
    public void DestroyBanner()
    {
        IronSource.Agent.destroyBanner();
    }

    void OnApplicationPause(bool isPaused) 
    {                 
        IronSource.Agent.onApplicationPause(isPaused);
    }
}
